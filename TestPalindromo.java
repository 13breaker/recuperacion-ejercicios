package interfaz;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class TestPalindromo {

	@Test
	void testPalindromo_test() {
		assertTrue(Palindromo.palindromo("02022020"));
	}
		
	@Test
	void testPalindromo_testF() {
		assertFalse(Palindromo.palindromo("02022021"));
	}
		
	@Test
	void testPalindromo_test2() {
		assertTrue(Palindromo.palindromo("orejero"));
	}
		
	@Test
	void testPalindromo_test2F() {
		assertFalse(Palindromo.palindromo("orejera"));
	}


}
